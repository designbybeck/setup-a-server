# Run Updates
// runs updates and confirms with yes. Removes other things

`sudo apt update -y && sudo apt full-upgrade -y`

# Set Time Zone and Date
// tzdata or timedatectl list-timezones
`dpkg-reconfigure tzdata`

# Setup Super User
// adds a new user. It will prompt you for info and password

`sudo adduser USERNAME`

// adds USERNAME to the sudo group

`sudo usermod -aG sudo USERNAME`

# Install Security Features
// already installed in Ubuntu. Installs security updates automatically 

`sudo apt install unattended-upgrades`

`sudo dpkg-reconfigure --priority=low unattended-upgrades`

# Setup SSH Keys
`ssh-keygen -b 4096 -C "InfoAboutThisKeyAndServer - 4CPU 4GB 80GB"`

# Setup Docker & Docker-Compose

## Docker Prereqs 
`sudo apt update`
`sudo apt-get install ca-certificates curl gnupg`

## Add Docker’s official GPG key
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

## Setup Dockers Repo
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

## Docker Install
`sudo apt update`
`sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin`

## Docker-Compose Install
`curl -SL https://github.com/docker/compose/releases/download/v2.18.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose`


# Setup Traefik Edge Router

https://gitlab.com/AdamBark/edgerouter
